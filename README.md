# Star Fighter

Star Fighter was a little game I made for the Game Design course at [Utrecht University](http://www.uu.nl/) in 2005.
It was made in [GameMaker](https://www.yoyogames.com/studio/), while the graphics were modelled and rendered in [Blender](http://www.blender.org/).

As a way to get acquainted with [Unity](http://unity3d.com/) I decided to rebuild this game from scratch.
A preview of the current state can be found [here](http://malienkolders.nl/jogchem/starfighter/ "Open in web player").