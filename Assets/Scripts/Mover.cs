﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {

	private GameController gameController;
	
	void Start () {
		gameController = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
		rigidbody.velocity = transform.forward * gameController.speed;
	}

}
