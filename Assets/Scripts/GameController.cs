﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public GUIText scoreText;

	public float speed = 8;
	
	private bool gameIsRunning = true;

	private int score;

	void Start () {
		score = 0;
		UpdateScore ();
	}

	void Update () {

	}

	public void IncScore() {
		score++;
		UpdateScore ();
	}

	void UpdateScore() {
		scoreText.text = "Stars: " + score;
	}

	public void StopTheGame() {
		gameIsRunning = false;
		foreach (GameObject tower in GameObject.FindGameObjectsWithTag("Tower")) {
			try {
				tower.rigidbody.velocity = Vector3.zero;
				tower.rigidbody.useGravity = true;
			} catch {}
		}
		foreach (GameObject pickup in GameObject.FindGameObjectsWithTag("Pickup")) {
			try {
				pickup.rigidbody.velocity = Vector3.zero;
				pickup.rigidbody.useGravity = true;
			} catch {}
		}
	}

	public bool isGameRunning() {
		return gameIsRunning;
	}
}
