﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;
	public float tilt;

	private bool inputEnabled = true;
	private GameController gameController;
	private bool visible = true;

	void Start() {
		Debug.Log ("Starting...");
		gameController = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
	}

	void FixedUpdate() {
		if (inputEnabled) {
			switch (Settings.Instance.inputControls) {
			case InputControls.Keyboard:
				float horMove = Input.GetAxis ("Horizontal");
				float vertMove = Input.GetAxis ("Vertical");
				rigidbody.AddForce (new Vector3 (-horMove, 0, -1 * vertMove) * speed);
				break;
			case InputControls.Mouse:
				Plane playerPlane = new Plane(Vector3.up, Vector3.zero);
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				float hitDistance = 0.0f;
				if (playerPlane.Raycast(ray, out hitDistance)) {
					Vector3 mousePosition = ray.GetPoint(hitDistance);
					Vector3 playerPosition = transform.position;
					playerPosition.y = 0;

					Vector3 difference = mousePosition - playerPosition;
					float strength = difference.magnitude * 5;

					Vector3 force = difference.normalized * strength;
					force.y = 0;
					rigidbody.AddForce(force);
				}
				break;
			}

			rigidbody.rotation = Quaternion.Euler (0, 0, rigidbody.velocity.x * tilt * -1);
			
			if (!visible) {
				Vector3 horizontalPosition = rigidbody.position;
				horizontalPosition.y = 0;
				rigidbody.AddForce (horizontalPosition.normalized * -speed);
			}
		}
		
	}

	void OnCollisionEnter(Collision col) {
		if (inputEnabled) {
			audio.Play();

			StopAllCoroutines();
			StartCoroutine(EndGame());

			rigidbody.useGravity = true;
			inputEnabled = false;
			gameController.StopTheGame();
			foreach (GameObject tower in GameObject.FindGameObjectsWithTag("Tower")) {
				tower.rigidbody.AddExplosionForce(1000, col.contacts[0].point, 40);
			}
			rigidbody.AddExplosionForce(1000, col.contacts[0].point, 40);
		}
	}

	void OnBecameVisible() {
		visible = true;
	}

	void OnBecameInvisible() {
		visible = false;
	}

	IEnumerator EndGame() {
		yield return new WaitForSeconds(1.0f);
		while (rigidbody.velocity.magnitude > 0.5f) {
			yield return new WaitForSeconds (1.0f);
		}
		Application.LoadLevel (0);
	}
}
