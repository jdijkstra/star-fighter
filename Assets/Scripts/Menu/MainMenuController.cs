﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	public float speed = 1;

	void ShowSettings () {
		StopAllCoroutines ();
		StartCoroutine (Rise ());
	}
	
	void HideSettings () {
		StopAllCoroutines ();
		StartCoroutine (Lower ());
	}
	
	IEnumerator Rise() {
		while(transform.position.y < 0.9f) {
			transform.Translate(Vector3.up * Mathf.Min(speed * Time.deltaTime, 0.9f - transform.position.y));
			yield return null;
		}
	}
	IEnumerator Lower() {
		while(transform.position.y > 0f) {
			transform.Translate(Vector3.up * Mathf.Max(-speed * Time.deltaTime, -transform.position.y));
			yield return null;
		}
	}
}
