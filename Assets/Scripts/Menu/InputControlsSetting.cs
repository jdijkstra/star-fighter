﻿using UnityEngine;
using System.Collections;

public class InputControlsSetting : MonoBehaviour {

	public GameObject mainMenu;

	public InputControls inputControls;

	void Start() {
		if (Settings.Instance.inputControls == inputControls) {
			renderer.material.color = Color.black;
		} else {
			renderer.material.color = Color.gray;
		}
	}

	void OnMouseEnter() {
		renderer.material.color = Color.white;
	}
	
	void OnMouseExit() {
		renderer.material.color = Color.black;
	}

	void OnMouseUp() {
		Settings.Instance.inputControls = inputControls;
		mainMenu.SendMessage ("HideSettings");
	}
}
