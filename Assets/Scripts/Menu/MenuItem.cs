﻿using UnityEngine;
using System.Collections;

public class MenuItem : MonoBehaviour {

	void Start() {
		renderer.material.color = Color.black;
	}

	void OnMouseEnter() {
		renderer.material.color = Color.white;
	}

	void OnMouseExit() {
		renderer.material.color = Color.black;
	}

}
