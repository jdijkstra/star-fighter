﻿using UnityEngine;
using System.Collections;

public class QuitGame : MonoBehaviour {

	void Start() {
		if (Application.isWebPlayer) {
			Destroy(gameObject);
		}
	}

	void OnMouseUp() {
		Application.Quit ();
	}
}
