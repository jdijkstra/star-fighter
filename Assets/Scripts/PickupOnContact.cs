﻿using UnityEngine;
using System.Collections;

public class PickupOnContact : MonoBehaviour {

	private GameController gameController;

	void Start() {
		gameController = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			audio.Play();
			gameController.IncScore();
			gameObject.rigidbody.AddForce(Vector3.up * 80);
			gameObject.rigidbody.AddTorque(Vector3.up * 100);
		}
	}

}
