﻿using UnityEngine;
using System.Collections;

public enum InputControls {Mouse, Keyboard};

public class Settings : Singleton<Settings> {

	protected Settings () {}

	public InputControls inputControls = InputControls.Mouse;
}
