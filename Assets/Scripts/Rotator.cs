﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

	public float speed = 15;

	void Start () {
		rigidbody.angularVelocity = Vector3.up * speed;
	}

}
