﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject harmlessTower;
	public GameObject killerTower;
	public GameObject pickup;
	
	public float spawnRate = 1f;
	public float spawnChance = 0.25f;

	private bool starting = true;

	private float nextSpawn = 0;
	
	private GameController gameController;
	
	void Start () {
		gameController = GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
	}
	
	void Update () {
		transform.Translate (Vector3.forward * gameController.speed * Time.deltaTime);

		if (transform.position.z > -30) {
			transform.Translate(new Vector3(0, 0, -10));
			Debug.Log("spawn point now at " + transform.position.z);
		}

		if (gameController.isGameRunning() && Time.time > nextSpawn) {
			nextSpawn = Time.time + spawnRate + Random.value * spawnRate;
			GameObject spawn = harmlessTower;
			if (starting) {
				starting = false;

				float x = transform.position.x;
				float z = transform.position.z;
				drawText (x, z, "GET");

				drawText (x +  5, z - 10, "READY");

				nextSpawn = Time.time + 5;
			} else {
				if (Random.value < 0.1) {
					spawn = pickup;
				} else if (Random.value < spawnChance) {
					spawn = killerTower;
				}
				Instantiate (spawn, new Vector3(Mathf.Floor(-30 + Random.value * 40), 0, transform.position.z), Quaternion.identity);
			}
		}
	}

	void drawPixel (float x, float z) {
		Instantiate(harmlessTower, new Vector3(x, 0, z), Quaternion.identity);
	}

	int[,,] letters = new int[26,5,4] {
		// A
		{{0, 1, 1, 0},
		 {1, 0, 0, 1},
		 {1, 1, 1, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1}},
		// B
		{{1, 1, 1, 0},
		 {1, 0, 0, 1},
		 {1, 1, 1, 0},
		 {1, 0, 0, 1},
		 {1, 1, 1, 0}},
		// C
		{{1, 1, 1, 1},
		 {1, 0, 0, 0},
		 {1, 0, 0, 0},
		 {1, 0, 0, 0},
		 {1, 1, 1, 1}},
		// D
		{{1, 1, 1, 0},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 1, 1, 0}},
		// E
		{{1, 1, 1, 1},
		 {1, 0, 0, 0},
		 {1, 1, 1, 0},
		 {1, 0, 0, 0},
		 {1, 1, 1, 1}},
		// F
		{{1, 1, 1, 1},
		 {1, 0, 0, 0},
		 {1, 1, 1, 0},
		 {1, 0, 0, 0},
		 {1, 0, 0, 0}},
		// G
		{{1, 1, 1, 1},
		 {1, 0, 0, 0},
		 {1, 0, 1, 1},
		 {1, 0, 0, 1},
		 {1, 1, 1, 1}},
		// H
		{{1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 1, 1, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1}},
		// I
		{{1, 1, 1, 0},
		 {0, 1, 0, 0},
		 {0, 1, 0, 0},
		 {0, 1, 0, 0},
		 {1, 1, 1, 0}},
		// J
		{{1, 1, 1, 1},
		 {0, 0, 0, 1},
		 {0, 0, 1, 1},
		 {1, 0, 0, 1},
		 {0, 1, 1, 0}},
		// K
		{{1, 0, 0, 1},
		 {1, 0, 1, 0},
		 {1, 1, 0, 0},
		 {1, 0, 1, 0},
		 {1, 0, 0, 1}},
		// L
		{{1, 0, 0, 0},
		 {1, 0, 0, 0},
		 {1, 0, 0, 0},
		 {1, 0, 0, 0},
		 {1, 1, 1, 1}},
		// M
		{{1, 0, 0, 1},
		 {1, 1, 1, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1}},
		// N
		{{1, 0, 0, 1},
		 {1, 1, 0, 1},
		 {1, 0, 1, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1}},
		// O
		{{0, 1, 1, 0},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {0, 1, 1, 0}},
		// P
		{{1, 1, 1, 0},
		 {1, 0, 0, 1},
		 {1, 1, 1, 0},
		 {1, 0, 0, 0},
		 {1, 0, 0, 0}},
		// Q
		{{0, 1, 1, 0},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {0, 1, 1, 0},
		 {0, 0, 0, 1}},
		// R
		{{1, 1, 1, 0},
		 {1, 0, 0, 1},
		 {1, 1, 1, 0},
		 {1, 0, 1, 0},
		 {1, 0, 0, 1}},
		// S
		{{0, 1, 1, 1},
		 {1, 0, 0, 0},
		 {0, 1, 1, 0},
		 {0, 0, 0, 1},
		 {1, 1, 1, 0}},
		// T
		{{1, 1, 1, 1},
		 {0, 1, 0, 0},
		 {0, 1, 0, 0},
		 {0, 1, 0, 0},
		 {0, 1, 0, 0}},
		// U
		{{1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {0, 1, 1, 0}},
		// V
		{{1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 0, 1, 0},
		 {0, 1, 0, 0}},
		// W
		{{1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 0, 0, 1},
		 {1, 1, 1, 1},
		 {1, 0, 0, 1}},
		// X
		{{1, 1, 0, 1},
		 {0, 0, 1, 0},
		 {0, 1, 1, 0},
		 {0, 1, 0, 0},
		 {1, 0, 1, 1}},
		// Y
		{{1, 0, 0, 1},
		 {0, 1, 0, 1},
		 {0, 0, 1, 0},
		 {0, 1, 0, 0},
		 {1, 0, 0, 0}},
		// Z
		{{1, 1, 1, 1},
		 {0, 0, 0, 1},
		 {0, 0, 1, 0},
		 {0, 1, 0, 0},
		 {1, 1, 1, 1}}
	};

	void drawText (float x, float z, string text) {
		float lx = x;
		for (int i = 0; i < text.Length; i++) {
			if (text[i] != ' ') {
				drawLetter(lx, z, text[i]);
			}
           lx -= 5;
        }
	}

	void drawLetter (float x, float z, char letter) {
		drawLetter (x, z, (int)letter - (int) 'A');
	}

	void drawLetter (float x, float z, int letterIndex) {
		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 4; i++) {
				if (letters[letterIndex, j, i] > 0) {
					drawPixel(x - i, z + j);
				}
			}
		}
	}

}
